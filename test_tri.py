from .tri import tri


def test_tri():
    liste = [42, -31, 3468, 743, 43, 9345]
    liste = tri(liste)
    for i in range(1, len(liste)):
        assert liste[i-1] < liste[i]
