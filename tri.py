

def fusion(a, b):
    c = []
    while len(a) > 0 and len(b) > 0:
        if a[0] < b[0]:
            c = c + [a.pop(0)]
        else:
            c = c + [b.pop(0)]
    c = c + a + b
    return c


def tri(liste):
    n = len(liste)
    if n > 2:
        return fusion(tri(liste[:n//2]), tri(liste[n//2:]))
    elif n == 2:
        if liste[0] > liste[1]:
            liste.reverse()
        return liste
    else:
        return liste


print(tri([8, 34834, 23, 43, 2, -43]))
